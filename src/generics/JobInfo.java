package generics;

import java.util.ArrayList;
import java.util.List;

public abstract class JobInfo<T extends PageInfo> {

    private List<T> pages = new ArrayList<T>();

    public JobInfo() {
        super();
    }

    public List<T> getPages() {
        return this.pages;
    }

    public void setPages(List<T> pages) {
        this.pages = pages;
    }

    public void addPage(T page) {
        this.pages.add(page);
    }

}
