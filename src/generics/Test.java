package generics;

import java.util.Iterator;

public class Test {

    public static void main(String[] args) {
        BuzzJobInfo job = new BuzzJobInfo();

        BuzzPageInfo page1 = new BuzzPageInfo("p1");
        BuzzPageInfo page2 = new BuzzPageInfo("p2");
        BuzzPageInfo page3 = new BuzzPageInfo("p3");

        job.addPage(page1);
        job.addPage(page2);
        job.addPage(page3);

        Iterator<BuzzPageInfo> it = job.getPages().iterator();
        while (it.hasNext()) {
            System.out.println(it.next().getName());
        }
    }

}
