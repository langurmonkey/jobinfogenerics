package generics;

public class PageInfo<T extends JobInfo> {
    private final String name;
    private T job;

    public PageInfo(String name) {
        this.name = name;
    }

    public T getJob() {
        return this.job;
    }

    public void setJob(T job) {
        this.job = job;
    }

    public String getName() {
        return this.name;
    }
}
